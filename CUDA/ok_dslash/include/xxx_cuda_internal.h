#ifndef __xxx_cuda_internal_H__
#define __xxx_cuda_internal_H__

#include <stdio.h>
#include <cuda_runtime.h>
#include "xxx_cuda_device_param.h"
#include "xxx_profile.h"

#if GLA_PrecisionInt == 1
#  define GLA_Real float
#  define GLA_Complex fComplex
#  define GLA_ColorVector fColorVector
#  define GLA_ColorMatrix fColorMatrix
#  define GLA_V_eqp_M_times_V F3_V_eqp_M_times_V
#  define GLA_V_eqm_M_times_V F3_V_eqm_M_times_V
#  define GLA_V_eqp_Ma_times_V F3_V_eqp_Ma_times_V
#  define GLA_V_eqm_Ma_times_V F3_V_eqm_Ma_times_V
#  define GLA_V_peq_M_times_V F3_V_peq_M_times_V
#  define GLA_V_meq_M_times_V F3_V_meq_M_times_V
#  define GLA_V_peq_Ma_times_V F3_V_peq_Ma_times_V
#  define GLA_V_meq_Ma_times_V F3_V_meq_Ma_times_V
//#  define okaction_dslash_mem_set_cuda F3_okaction_dslash_mem_set_cuda
//#  define okaction_dslash_mem_free_cuda F3_okaction_dslash_mem_free_cuda
#  define okaction_dslash_kernel_cuda F3_okaction_dslash_kernel_cuda
#else
#  define GLA_Real double
#  define GLA_Complex dComplex
#  define GLA_ColorVector dColorVector
#  define GLA_ColorMatrix dColorMatrix
#  define GLA_V_eqp_M_times_V D3_V_eqp_M_times_V
#  define GLA_V_eqm_M_times_V D3_V_eqm_M_times_V
#  define GLA_V_eqp_Ma_times_V D3_V_eqp_Ma_times_V
#  define GLA_V_eqm_Ma_times_V D3_V_eqm_Ma_times_V
#  define GLA_V_peq_M_times_V D3_V_peq_M_times_V
#  define GLA_V_meq_M_times_V D3_V_meq_M_times_V
#  define GLA_V_peq_Ma_times_V D3_V_peq_Ma_times_V
#  define GLA_V_meq_Ma_times_V D3_V_meq_Ma_times_V
//#  define okaction_dslash_mem_set_cuda D3_okaction_dslash_mem_set_cuda
//#  define okaction_dslash_mem_free_cuda D3_okaction_dslash_mem_free_cuda
#  define okaction_dslash_kernel_cuda D3_okaction_dslash_kernel_cuda
#endif


#ifdef __cplusplus
extern "C" {
#endif

typedef struct _f_complex{
  float Re;
  float Im;
} fComplex; 
//__attribute__ ((aligned (8)));

typedef struct _d_complex{
  double Re;
  double Im;
} dComplex; 
//__attribute__ ((aligned (16)));

typedef struct _f_colorvector{
  float c0Re;
  float c0Im;
  float c1Re;
  float c1Im;
  float c2Re;
  float c2Im;
} fColorVector; 
//__attribute__ ((aligned (24)));

typedef struct _d_colorvector{
  double c0Re;
  double c0Im;
  double c1Re;
  double c1Im;
  double c2Re;
  double c2Im;
} dColorVector; 
//__attribute__ ((aligned (48)));

typedef struct _f_colormatrix{
  float c00Re;
  float c00Im;
  float c01Re;
  float c01Im;
  float c02Re;
  float c02Im;
  float c10Re;
  float c10Im;
  float c11Re;
  float c11Im;
  float c12Re;
  float c12Im;
  float c20Re;
  float c20Im;
  float c21Re;
  float c21Im;
  float c22Re;
  float c22Im;
} fColorMatrix; 
//__attribute__ ((aligned (72)));

typedef struct _d_colormatrix{
  double c00Re;
  double c00Im;
  double c01Re;
  double c01Im;
  double c02Re;
  double c02Im;
  double c10Re;
  double c10Im;
  double c11Re;
  double c11Im;
  double c12Re;
  double c12Im;
  double c20Re;
  double c20Im;
  double c21Re;
  double c21Im;
  double c22Re;
  double c22Im;
} dColorMatrix;
//__attribute__ ((aligned (144)));

extern double *d_Mat[25];
extern double *d_Vec[48];
extern int cuda_volume;
extern int cuda_volume_tail;
extern int cuda_precision;
const int CUDA_SINGLE_IN_DOUBLE = 0;
const int CUDA_SINGLE = 1;
const int CUDA_DOUBLE = 2;

void CheckCUDA(cudaError_t status);
//void okaction_dslash_mem_set_cuda( int sites_on_node );
//void okaction_dslash_mem_free_cuda();

#ifdef __cplusplus
}
#endif


#endif
