#ifndef __xxx_cuda_milc_interface_H__
#define __xxx_cuda_milc_interface_H__

//#include "xxx_cuda_device_param.h"

#ifdef __cplusplus
extern "C" {
#endif

void okaction_dslash_mem_set_cuda( int sites_on_node );

void okaction_dslash_mem_free_cuda();

#ifdef __cplusplus
}
#endif

#endif
