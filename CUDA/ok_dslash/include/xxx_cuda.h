#ifndef __xxx_cuda_H__
#define __xxx_cuda_H__

//#include "xxx_cuda_device_param.h"

#ifdef __cplusplus
extern "C" {
#endif

//void okaction_dslash_mem_set_cuda( int sites_on_node );

//void okaction_dslash_mem_free_cuda();

void D3_okaction_dslash_kernel_cuda( 
  double *h_kap,
  double *h_D0[], double *h_S0[], 
  double *h_U3, 
  double *h_Dp0[], double *h_Dp1[], double *h_Dp2[],
  double *h_Sp0[], double *h_Sp1[], double *h_Sp2[], double *h_Sp3[],
  double *h_Dpp[], double *h_Spp[], double *Spm[],
  double *h_src[], 
  double *h_fwd0[], double *h_fwd1[], double *h_fwd2[], double *h_fwd3[], 
  double *h_bck0[], double *h_bck1[], double *h_bck2[], 
  double *h_ans[], 
  double *h_push0[], double *h_push1[], double *h_push2[], 
  double *h_pull0[], double *h_pull1[], double *h_pull2[], double *h_pull3[],
  int sites_on_node, int arrange );

void F3_okaction_dslash_kernel_cuda(
  float *h_kap,
  float *h_D0[], float *h_S0[], 
  float *h_U3, 
  float *h_Dp0[], float *h_Dp1[], float *h_Dp2[],
  float *h_Sp0[], float *h_Sp1[], float *h_Sp2[], float *h_Sp3[],
  float *h_Dpp[], float *h_Spp[], float *Spm[],
  float *h_src[], 
  float *h_fwd0[], float *h_fwd1[], float *h_fwd2[], float *h_fwd3[], 
  float *h_bck0[], float *h_bck1[], float *h_bck2[], 
  float *h_ans[], 
  float *h_push0[], float *h_push1[], float *h_push2[], 
  float *h_pull0[], float *h_pull1[], float *h_pull2[], float *h_pull3[], 
  int sites_on_node, int arrange );

#ifdef __cplusplus
}
#endif

#endif
