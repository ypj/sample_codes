#ifndef __XXX_PROFILE_H__
#define __XXX_PROFILE_H__

#include <sys/time.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

struct xxx_profile{
  int addtime;
  struct timeval last_stamp;
  double elapsed;
};

int XXX_profile(struct xxx_profile *pf);
int XXX_profile_init(struct xxx_profile *pf);

#ifdef __cplusplus
}
#endif

#endif
