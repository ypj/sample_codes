#include "xxx_cuda_internal.h"

//=========================================================================
// Color matrix multiplication modules with color vector
//
//=========================================================================
__device__ void GLA_V_peq_M_times_V( GLA_Complex * __restrict__ a, 
                                     GLA_Complex * __restrict__ b, 
                                     GLA_Complex * __restrict__ c )
{
  a[0].Re += (  b[0].Re * c[0].Re - b[0].Im * c[0].Im
              + b[1].Re * c[1].Re - b[1].Im * c[1].Im
              + b[2].Re * c[2].Re - b[2].Im * c[2].Im );
  
  a[0].Im += (  b[0].Re * c[0].Im + b[0].Im * c[0].Re
              + b[1].Re * c[1].Im + b[1].Im * c[1].Re
              + b[2].Re * c[2].Im + b[2].Im * c[2].Re );

  a[1].Re += (  b[3].Re * c[0].Re - b[3].Im * c[0].Im
              + b[4].Re * c[1].Re - b[4].Im * c[1].Im
              + b[5].Re * c[2].Re - b[5].Im * c[2].Im );
  
  a[1].Im += (  b[3].Re * c[0].Im + b[3].Im * c[0].Re
              + b[4].Re * c[1].Im + b[4].Im * c[1].Re
              + b[5].Re * c[2].Im + b[5].Im * c[2].Re );

  a[2].Re += (  b[6].Re * c[0].Re - b[6].Im * c[0].Im
              + b[7].Re * c[1].Re - b[7].Im * c[1].Im
              + b[8].Re * c[2].Re - b[8].Im * c[2].Im );
  
  a[2].Im += (  b[6].Re * c[0].Im + b[6].Im * c[0].Re
              + b[7].Re * c[1].Im + b[7].Im * c[1].Re
              + b[8].Re * c[2].Im + b[8].Im * c[2].Re );
}

__device__ void GLA_V_meq_M_times_V( GLA_Complex * __restrict__ a, 
                                     GLA_Complex * __restrict__ b, 
                                     GLA_Complex * __restrict__ c )
{
  a[0].Re -= (  b[0].Re * c[0].Re - b[0].Im * c[0].Im
              + b[1].Re * c[1].Re - b[1].Im * c[1].Im
              + b[2].Re * c[2].Re - b[2].Im * c[2].Im );
  
  a[0].Im -= (  b[0].Re * c[0].Im + b[0].Im * c[0].Re
              + b[1].Re * c[1].Im + b[1].Im * c[1].Re
              + b[2].Re * c[2].Im + b[2].Im * c[2].Re );

  a[1].Re -= (  b[3].Re * c[0].Re - b[3].Im * c[0].Im
              + b[4].Re * c[1].Re - b[4].Im * c[1].Im
              + b[5].Re * c[2].Re - b[5].Im * c[2].Im );
  
  a[1].Im -= (  b[3].Re * c[0].Im + b[3].Im * c[0].Re
              + b[4].Re * c[1].Im + b[4].Im * c[1].Re
              + b[5].Re * c[2].Im + b[5].Im * c[2].Re );

  a[2].Re -= (  b[6].Re * c[0].Re - b[6].Im * c[0].Im
              + b[7].Re * c[1].Re - b[7].Im * c[1].Im
              + b[8].Re * c[2].Re - b[8].Im * c[2].Im );
  
  a[2].Im -= (  b[6].Re * c[0].Im + b[6].Im * c[0].Re
              + b[7].Re * c[1].Im + b[7].Im * c[1].Re
              + b[8].Re * c[2].Im + b[8].Im * c[2].Re );
}

__device__ void GLA_V_peq_Ma_times_V( GLA_Complex * __restrict__ a, 
                                      GLA_Complex * __restrict__ b, 
                                      GLA_Complex * __restrict__ c )
{
  a[0].Re += (  b[0].Re * c[0].Re + b[0].Im * c[0].Im
              + b[3].Re * c[1].Re + b[3].Im * c[1].Im
              + b[6].Re * c[2].Re + b[6].Im * c[2].Im );
  
  a[0].Im += (  b[0].Re * c[0].Im - b[0].Im * c[0].Re
              + b[3].Re * c[1].Im - b[3].Im * c[1].Re
              + b[6].Re * c[2].Im - b[6].Im * c[2].Re );

  a[1].Re += (  b[1].Re * c[0].Re + b[1].Im * c[0].Im
              + b[4].Re * c[1].Re + b[4].Im * c[1].Im
              + b[7].Re * c[2].Re + b[7].Im * c[2].Im );
                                        
  a[1].Im += (  b[1].Re * c[0].Im - b[1].Im * c[0].Re
              + b[4].Re * c[1].Im - b[4].Im * c[1].Re
              + b[7].Re * c[2].Im - b[7].Im * c[2].Re );
                                        
  a[2].Re += (  b[2].Re * c[0].Re + b[2].Im * c[0].Im
              + b[5].Re * c[1].Re + b[5].Im * c[1].Im
              + b[8].Re * c[2].Re + b[8].Im * c[2].Im );
                                        
  a[2].Im += (  b[2].Re * c[0].Im - b[2].Im * c[0].Re
              + b[5].Re * c[1].Im - b[5].Im * c[1].Re
              + b[8].Re * c[2].Im - b[8].Im * c[2].Re );
}

__device__ void GLA_V_meq_Ma_times_V( GLA_Complex * __restrict__ a, 
                                      GLA_Complex * __restrict__ b, 
                                      GLA_Complex * __restrict__ c )
{
  a[0].Re -= (  b[0].Re * c[0].Re + b[0].Im * c[0].Im
              + b[3].Re * c[1].Re + b[3].Im * c[1].Im
              + b[6].Re * c[2].Re + b[6].Im * c[2].Im );
                                        
  a[0].Im -= (  b[0].Re * c[0].Im - b[0].Im * c[0].Re
              + b[3].Re * c[1].Im - b[3].Im * c[1].Re
              + b[6].Re * c[2].Im - b[6].Im * c[2].Re );
                                        
  a[1].Re -= (  b[1].Re * c[0].Re + b[1].Im * c[0].Im
              + b[4].Re * c[1].Re + b[4].Im * c[1].Im
              + b[7].Re * c[2].Re + b[7].Im * c[2].Im );
                                        
  a[1].Im -= (  b[1].Re * c[0].Im - b[1].Im * c[0].Re
              + b[4].Re * c[1].Im - b[4].Im * c[1].Re
              + b[7].Re * c[2].Im - b[7].Im * c[2].Re );
                                        
  a[2].Re -= (  b[2].Re * c[0].Re + b[2].Im * c[0].Im
              + b[5].Re * c[1].Re + b[5].Im * c[1].Im
              + b[8].Re * c[2].Re + b[8].Im * c[2].Im );
                                        
  a[2].Im -= (  b[2].Re * c[0].Im - b[2].Im * c[0].Re
              + b[5].Re * c[1].Im - b[5].Im * c[1].Re
              + b[8].Re * c[2].Im - b[8].Im * c[2].Re );
}

//=========================================================================
// Kernel for 0-hop term multiplication
//
//=========================================================================
__global__ void W0
( GLA_Real *d_ans_s0, GLA_Real *d_ans_s1, 
  GLA_Real *d_ans_s2, GLA_Real *d_ans_s3, 
  GLA_Real *d_D0_b0, GLA_Real *d_D0_b1, 
  GLA_Real *d_S0_b0, GLA_Real *d_S0_b1,
  GLA_Real *d_src_s0, GLA_Real *d_src_s1, 
  GLA_Real *d_src_s2, GLA_Real *d_src_s3,
  int cuda_volume )
{

  //=================
  // Local Variables
  //=================
  //int tpb = blockDim.x;   // number of threads per block
  int tx = threadIdx.x;
  int site = THREAD*blockIdx.x + tx;
  int siteV = 6*THREAD*blockIdx.x + tx;
  int siteM = 18*THREAD*blockIdx.x + tx;
  int offset;

  int idx, ic, jc, s;

  GLA_Complex ans_s0[3];
  GLA_Complex ans_s1[3];
  GLA_Complex ans_s2[3];
  GLA_Complex ans_s3[3];
  GLA_Complex src_s0[3];
  GLA_Complex src_s1[3];
  GLA_Complex src_s2[3];
  GLA_Complex src_s3[3];
  GLA_Complex D0_b0[9];
  GLA_Complex D0_b1[9];
  GLA_Complex S0_b0[9];
  GLA_Complex S0_b1[9];

  GLA_Complex complex_zero;
  complex_zero.Re = 0.;
  complex_zero.Im = 0.;

  __shared__ GLA_Real s_tmpV[THREAD*6], s_tmpM[THREAD*18];

  //================================================ 
  // Read QLA_ColorMatrix Through the Shared Memory
  //================================================ 
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmpM[tx+THREAD*idx] = d_D0_b0[siteM+THREAD*idx];
  }
  __syncthreads();

  #pragma unroll
  for( ic = 0; ic < 9; ic++ ){
    D0_b0[ic] = ((GLA_Complex *)s_tmpM)[9*tx+ic];
  }
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmpM[tx+THREAD*idx] = d_D0_b1[siteM+THREAD*idx];
  }
  __syncthreads();

  #pragma unroll
  for( ic = 0; ic < 9; ic++ ){
    D0_b1[ic] = ((GLA_Complex *)s_tmpM)[9*tx+ic];
  }
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmpM[tx+THREAD*idx] = d_S0_b0[siteM+THREAD*idx];
  }
  __syncthreads();

  #pragma unroll
  for( ic = 0; ic < 9; ic++ ){
    S0_b0[ic] = ((GLA_Complex *)s_tmpM)[9*tx+ic];
  }
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmpM[tx+THREAD*idx] = d_S0_b1[siteM+THREAD*idx];
  }
  __syncthreads();

  #pragma unroll
  for( ic = 0; ic < 9; ic++ ){
    S0_b1[ic] = ((GLA_Complex *)s_tmpM)[9*tx+ic];
  }
  __syncthreads();
  
  //======================================================= 
  // Read Source QLA_ColorVector Through the Shared Memory
  //======================================================= 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmpV[tx+THREAD*idx] = d_src_s0[siteV+THREAD*idx];
  }
  __syncthreads();

  #pragma unroll
  for( ic = 0; ic < 3; ic++ ){
    src_s0[ic] = ((GLA_Complex *)s_tmpV)[3*tx+ic];
  }
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmpV[tx+THREAD*idx] = d_src_s1[siteV+THREAD*idx];
  }
  __syncthreads();

  #pragma unroll
  for( ic = 0; ic < 3; ic++ ){
    src_s1[ic] = ((GLA_Complex *)s_tmpV)[3*tx+ic];
  }
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmpV[tx+THREAD*idx] = d_src_s2[siteV+THREAD*idx];
  }
  __syncthreads();

  #pragma unroll
  for( ic = 0; ic < 3; ic++ ){
    src_s2[ic] = ((GLA_Complex *)s_tmpV)[3*tx+ic];
  }
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmpV[tx+THREAD*idx] = d_src_s3[siteV+THREAD*idx];
  }
  __syncthreads();

  #pragma unroll
  for( ic = 0; ic < 3; ic++ ){
    src_s3[ic] = ((GLA_Complex *)s_tmpV)[3*tx+ic];
  }
  __syncthreads();

  //============ 
  // Initialize
  //============ 
  #pragma unroll
  for( ic = 0; ic < 3; ic++ ){
    ans_s0[ic] = complex_zero;
    ans_s1[ic] = complex_zero;
    ans_s2[ic] = complex_zero;
    ans_s3[ic] = complex_zero;
  }
  
  //==========================
  // W0 part of Dslash Kernel
  //==========================
  GLA_V_peq_M_times_V(ans_s0,D0_b0,src_s0); 
  GLA_V_peq_M_times_V(ans_s0,D0_b1,src_s1); 
  GLA_V_peq_M_times_V(ans_s0,S0_b0,src_s2); 
  GLA_V_peq_M_times_V(ans_s0,S0_b1,src_s3);

  GLA_V_peq_Ma_times_V(ans_s1,D0_b1,src_s0);
  GLA_V_meq_Ma_times_V(ans_s1,D0_b0,src_s1);
  GLA_V_meq_Ma_times_V(ans_s1,S0_b1,src_s2);
  GLA_V_peq_Ma_times_V(ans_s1,S0_b0,src_s3);
  
  GLA_V_meq_M_times_V(ans_s2,S0_b0,src_s0); 
  GLA_V_meq_M_times_V(ans_s2,S0_b1,src_s1); 
  GLA_V_peq_M_times_V(ans_s2,D0_b0,src_s2); 
  GLA_V_peq_M_times_V(ans_s2,D0_b1,src_s3);

  GLA_V_peq_Ma_times_V(ans_s3,S0_b1,src_s0);
  GLA_V_meq_Ma_times_V(ans_s3,S0_b0,src_s1);
  GLA_V_peq_Ma_times_V(ans_s3,D0_b1,src_s2);
  GLA_V_meq_Ma_times_V(ans_s3,D0_b0,src_s3);

  __syncthreads();

  //======================================================= 
  // Write Result QLA_ColorVector Through the Shared Memory
  //======================================================= 
  #pragma unroll
  for( ic = 0; ic < 3; ic++ ){
    ((GLA_Complex *)s_tmpV)[3*tx+ic] = ans_s0[ic];
  }
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s0[siteV+THREAD*idx] = s_tmpV[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( ic = 0; ic < 3; ic++ ){
    ((GLA_Complex *)s_tmpV)[3*tx+ic] = ans_s1[ic];
  }
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s1[siteV+THREAD*idx] = s_tmpV[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( ic = 0; ic < 3; ic++ ){
    ((GLA_Complex *)s_tmpV)[3*tx+ic] = ans_s2[ic];
  }
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s2[siteV+THREAD*idx] = s_tmpV[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( ic = 0; ic < 3; ic++ ){
    ((GLA_Complex *)s_tmpV)[3*tx+ic] = ans_s3[ic];
  }
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s3[siteV+THREAD*idx] = s_tmpV[tx+THREAD*idx];
  }
  __syncthreads();

/*
  for( ic = 0; ic < 6; ic++ ){
    // *( d_ans[s] + ic*cuda_volume + site ) = ans[s][ic];
    offset = ic*THREAD + siteV;
    ans_s0[offset] = ic;
    ans_s1[offset] = ic+6;
    ans_s2[offset] = ic+12;
    ans_s3[offset] = ic+18;
  }
*/
}

//=========================================================================
// Interface between host(CPU) and device(GPU)
//
//=========================================================================
//extern "C" void mem_allocate_cuda();
//extern "C" void mem_free_cuda();
extern "C" void okaction_dslash_kernel_cuda( 
  GLA_Real *h_D0[], GLA_Real *h_S0[], 
  GLA_Real *h_U3, GLA_Real *h_Dp[][4], GLA_Real *h_Sp[][4],
  GLA_Real *h_Dpp[], GLA_Real *h_Spp[], GLA_Real *Spm[][5],
  GLA_Real *h_src[], GLA_Real *h_fwd[][4], GLA_Real *h_bck[][4], 
  GLA_Real *h_ans[], GLA_Real *h_push[][4], GLA_Real *h_pull[][4], 
  int this_node, int sites_on_node, int arrange )
{
  //=================
  // Local Variables
  //=================
  int s, idx;

  GLA_Real *d_tmpM1[4], *d_tmpM2[4];
  //GLA_Real *d_D0[2], *d_S0[2];
  GLA_Real *d_U3, *d_Dp[3][4], *d_Sp[4][4];
  GLA_Real *d_Dpp[3], *d_Spp[3], *d_Spm[2][5];
  
  GLA_Real *d_tmpV1[4], *d_tmpV2[4];
  //GLA_Real *d_src[4], *d_ans[4];
  GLA_Real *d_fwd[4][4], *d_bck[3][4];
  GLA_Real *d_push[3][4], *d_pull[4][4];

  //==============
  // Device Setup
  //==============
  int cuda_volume = sites_on_node;
  dim3 Nthread(THREAD,1,1);
  dim3 Nblock(cuda_volume/THREAD,1,1);
 
  cudaError_t check = cudaSuccess;
  size_t sizeV = sizeof(GLA_Real)*6*cuda_volume;
  size_t sizeM = sizeof(GLA_Real)*18*cuda_volume;

  int Ndevice;
  int this_device;

  //cudaGetDeviceCount(&Ndevice);
  //this_device = this_node % Ndevice;
  //cudaSetDevice(this_device);

  //==========================
  // Device Memory Allocation
  //==========================
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    check = cudaMalloc((void **)&d_tmpM1[s], sizeM );
    CUDA_ErrorExit(check);
    
    check = cudaMalloc((void **)&d_tmpM2[s], sizeM );
    CUDA_ErrorExit(check);
    
    check = cudaMalloc((void **)&d_tmpV1[s], sizeV );
    CUDA_ErrorExit(check);
    
    check = cudaMalloc((void **)&d_tmpV2[s], sizeV );
    CUDA_ErrorExit(check);
  }

  //============
  // 0-hop term
  //============
  #pragma unroll
  for( s = 0; s < 2; s++ ){
    check = cudaMemcpy(d_tmpM1[s], h_D0[s], sizeM, cudaMemcpyHostToDevice);
    CUDA_ErrorExit(check);
    
    check = cudaMemcpy(d_tmpM2[s], h_S0[s], sizeM, cudaMemcpyHostToDevice);
    CUDA_ErrorExit(check);
  }

  #pragma unroll
  for( s = 0; s < 4; s++ ){
    check = cudaMemcpy(d_tmpV1[s], h_src[s], sizeV, cudaMemcpyHostToDevice);
    CUDA_ErrorExit(check);

    //check = cudaMemset(d_tmpV2[s], 0, sizeV );
    //CUDA_ErrorExit(check);
  }

  W0<<< Nblock, Nthread >>>( d_tmpV2[0], d_tmpV2[1], d_tmpV2[2], d_tmpV2[3],
                             d_tmpM1[0], d_tmpM1[1], 
                             d_tmpM2[0], d_tmpM2[1],
                             d_tmpV1[0], d_tmpV1[1], d_tmpV1[2], d_tmpV1[3], 
                             cuda_volume );

  #pragma unroll
  for( s = 0; s < 4; s++ ){
    check = cudaMemcpy(h_ans[s], d_tmpV2[s], sizeV, cudaMemcpyDeviceToHost);
    CUDA_ErrorExit(check);
  }

#if 0
    printf( "okaction_dslash_kernel_cuda(): node = %d, device = %d\n", 
            this_node, this_device );
    
    if( this_node == 0 ){

      for( idx = 6*(cuda_volume-1); idx < 6*cuda_volume; idx++ ){
        for( s = 0; s < 4; s++ ){
          printf( "idx = %d, h_src[%d] = %le\n", 
                  idx, s, *(h_src[s]+idx) );
          
          printf( "idx = %d, h_ans[%d] = %le\n",
                  idx, s, *(h_ans[s]+idx) );
        }
      }

    }
#endif

//  Wp3<< Nblock, Nthread >>( d_ans, d_pull3, d_U3, d_Sp3, d_fwd, d_src, 
//                            cuda_vol_unit );
//  Wpi<< Nblock, Nthread >>( d_ans, d_pull0, d_Dp0, d_Sp0, d_fwd0, d_src, 
//                            cuda_vol_unit );
//  Wpi<< Nblock, Nthread >>( d_ans, d_pull1, d_Dp1, d_Sp1, d_fwd1, d_src, 
//                            cuda_vol_unit );
//  Wpi<< Nblock, Nthread >>( d_ans, d_pull2, d_Dp2, d_Sp2, d_fwd2, d_src, 
//                            cuda_vol_unit );
//  Wpp0<< Nblock, Nthread >>( d_push0, d_pull0,
//                            d_Dpp0, d_Spp0, 
//                            d_fwd0, d_bck0,
//                            cuda_vol_unit );
//  Wpp1<< Nblock, Nthread >>( d_push1, d_pull1,
//                            d_Dpp1, d_Spp1, 
//                            d_fwd1, d_bck1,
//                            cuda_vol_unit );
//  Wpp2<< Nblock, Nthread >>( d_push2, d_pull2,
//                            d_Dpp2, d_Spp2, 
//                            d_fwd2, d_bck2,
//                            cuda_vol_unit );
//  Wpm<< Nblock, Nthread >>( d_push, d_pull, d_Spm0, d_fwd, d_bck, 
//                            cuda_vol_unit );
//  Wpm<< Nblock, Nthread >>( d_push, d_pull, d_Spm1, d_fwd, d_bck, 
//                            cuda_vol_unit );

  //========================
  // Free the Device Memory
  //========================
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    cudaFree(d_tmpM1[s]);
    cudaFree(d_tmpM2[s]);
    cudaFree(d_tmpV1[s]);
    cudaFree(d_tmpV2[s]);
  }
  
  //cudaDeviceReset();

}
