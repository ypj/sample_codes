#include "xxx_cuda_internal.h"
#define NMAT 25
#define NVEC 48
double *d_Mat[NMAT];
double *d_Vec[NVEC];
int cuda_volume;
int cuda_volume_tail;
int cuda_precision;

extern "C" void okaction_dslash_mem_set_cuda( int sites_on_node )
{
//  if( sites_on_node % 2 ){
//    cuda_volume = (sites_on_node+1)/2;
//    cuda_volume_tail = cuda_volume-1;
//  }else{
//    cuda_volume = sites_on_node/2;
//    cuda_volume_tail = cuda_volume;
//  }
//
//  printf("cuda_volume = %d, cuda_volume_tail = %d, sites_on_node = %d\n",
//          cuda_volume, cuda_volume_tail, sites_on_node);
  cuda_volume = sites_on_node;
  cuda_precision = CUDA_SINGLE_IN_DOUBLE;

  size_t sizeVec = sizeof(double)*6*(cuda_volume);
  size_t sizeMat = sizeof(double)*18*(cuda_volume);
  
  int s;
  
  #pragma unroll
  for( s = 0; s < NMAT; s++ ){
    CheckCUDA(cudaMalloc((void **)&d_Mat[s], sizeMat ));
  }
  
  #pragma unroll
  for( s = 0; s < NVEC; s++ ){
    CheckCUDA(cudaMalloc((void **)&d_Vec[s], sizeVec ));
  }

  // DEBUG
  //fprintf(stdout,
  //        "cuda_volume = %d, sites_on_node = %d\n", cuda_volume, sites_on_node);
  //fflush(stdout);
}

extern "C" void okaction_dslash_mem_free_cuda(void)
{ 
  cuda_precision = CUDA_SINGLE_IN_DOUBLE;
  
  int s;

  #pragma unroll
  for( s = 0; s < NMAT; s++ ){
    cudaFree(d_Mat[s]);
  }
  
  #pragma unroll
  for( s = 0; s < NVEC; s++ ){
    cudaFree(d_Vec[s]);
  }
}

#undef NMAT
#undef NVEC
