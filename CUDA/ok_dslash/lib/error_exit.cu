#include "xxx_cuda_internal.h"

extern "C" void CheckCUDA(cudaError_t status)
{
  if( status != cudaSuccess ){
    fprintf(stderr, "CUDA_FAILURE (error code = %s)\n", 
            cudaGetErrorString(status));
    exit(EXIT_FAILURE);
  }
}
