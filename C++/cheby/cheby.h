//#include <cstddef>
#include <vector>
#include <functional>
#include <assert.h>
#include <math.h>

//-----------------------------------------
// emulate the CPS class PolyArg without VML
typedef enum {
  Cheby = 0,
} PolyType;


class PolyArg
{
  public:
    PolyType poly_type;
    double high;
    double low;
    struct {
      unsigned int coeffs_len;
      double *coeffs_val;
    } coeffs;
    
    PolyArg();
    ~PolyArg();
    int resize(int);
};
//-----------------------------------------


class ChebyApprox
{
  private:
    const char *cname = "ChebyApprox";
    //double *coeffs;
    std::vector<double> coeffs;
    double high, low;
    int N;

  public:
    ChebyApprox(); 
    ~ChebyApprox(); 
    
    ChebyApprox(int N_, double high_ = 1.0, double low_ = -1.0); 
    
    double T(int n, double x);

    double roots(int n, int k);

    double unscaled(double theta);
  
    double scaled(double x);

    void buildApprox(const std::function<double(double)> &func, 
                     PolyArg *poly_arg = nullptr);

    double approx(double x);
    double approx_inverse(double x) { return 1.0/approx(x); };

};


